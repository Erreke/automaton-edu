import firebase from "firebase/app";
import "firebase/firestore";

const config = {
  apiKey: "AIzaSyCwUTBtKPcUjQoco6MJSkVROmtgjbJmHs0",
  authDomain: "automaton-edu.firebaseapp.com",
  databaseURL: "https://automaton-edu.firebaseio.com",
  projectId: "automaton-edu",
  storageBucket: "automaton-edu.appspot.com",
  messagingSenderId: "299378615289",
  appId: "1:299378615289:web:34aa36f7655a719e339328",
  measurementId: "G-Q8JTB5MTXS"
};

export const firestoreApp = firebase.initializeApp(config);

firebase
  .firestore()
  .enablePersistence()
  .catch(error => {
    console.error(error);

    if (error.code === "unimplemented") {
      console.warn(
        "The current browser does not support all of the features required to enable persistence"
      );
    }
  });

export default firestoreApp.firestore();
